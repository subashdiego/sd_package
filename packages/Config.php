<?php

/**
 * @var [Config.php] 
 * @package [sdp/packages]
 * @author [Subashdiego]
 * @uses [this inner package is used to set cron]
 * @see [process all inforamation in Cron files]
 */

namespace sdp\packages;

class Config{

    /**
     * @see ------------------------
     * @var BACK_UP_CONFIGURATION
     * @see ------------------------
     */

    /**
     * @var backup_file_location
     * @uses backup_file_location used to get backup all files
     */

    public $backup_file_location;

    /**
     * @var db_backup_host_url
     * @uses db_backup_host_url is used to used to save backup 
     */
    public $db_backup_host_url;

    /**
     * @var db_backup_user
     * @uses db_backup_user is used to backup user for setting up backup
     */

     public $db_backup_user;

    /**
     * @var db_backup_password
     * @uses db_backup_password is used to backup passowrd for setting up backup
     */

    public $db_backup_password;

    /**
     * @var db_backup_databases
     * @uses db_backup_databases is used to backup databases in list
     */

    public $db_backup_databases = [];

    /**
     * @var db_backup_logs
     * @uses db_backup_logs is used to backup databases in list
     */

    public $db_backup_logs = FALSE;

    /**
     * @var backup_notify
     * @uses db_backup_notify is used to notify is enable or not
     */

    public $backup_notify = FALSE;

    /**
     * @var backup_notify_host
     * @uses backup_notify_host is used to notify email's host
     */

    public $backup_notify_host;

    /**
     * @var backup_notify_email
     * @uses backup_notify_email is used to notify through email
     */

    public $backup_notify_email;

    /**
     * @var backup_notify_password
     * @uses backup_notify_password is used to notify email password
     */

    public $backup_notify_password;

    /**
     * @var backup_notify_mail_type
     * @uses backup_notify_mail_type is used to notify email type is smtp or pop
     */

    public $backup_notify_mail_type = "SMTP";

    /**
     * @var backup_notify_host_port
     * @uses backup_notify_mail_type is used to notify email type is 25 or 465
     */

    public $backup_notify_host_port = "465";

    /**
     * @var backup_notify_host_port
     * @uses backup_notify_mail_type is used to notify email type is 25 or 465
     */

    public $backup_notify_host_port = "465";

    /**
     * @see ------------------------
     * @var CRON_SETUP_CONFIGURATION
     * @see ------------------------
     */

    /**
     * @see ------------------------
     * @var SMS_SETUP_CONFIGURATION
     * @see ------------------------
     */

    /**
     * @see ------------------------
     * @var CURL_CONFIGURATION
     * @see ------------------------
     */

    

}

?>