<?php

/**
 * Backup 
 *
 * Uses to do database backup using (mysql (cmd))
 * 
 *
 *
 * @package	Streamer
 * @author	Subash Diego
 * @version 0.1
 * @link http://www.facebook.com/schandar
 */

namespace sdp\packages\Backup;

use sdb\packages\Config;
use sdb\packages\Backup\Utility;

class Backup {

	/**
	*
	* @param backup_directory_url
	* 
	* (param used to check directory make backup)
	*/

	private $backup_directory_url;

	/**
	*
	* @param database_names
	* 
	* (param used to take backup to given list)
	*/
	
	private $database_names = [];

	/**
	*
	* @param db_user_host
	* 
	* (param used to access host of user)
	*/
	
	private $db_user_host;

	/**
	*
	* @param db_user_name
	* 
	* (param used to access mysql as database)
	*/
	
	private $db_user_name;

	/**
	*
	* @param db_user_pass
	* 
	* (param used to access mysql as database)
	*/
	
	private $db_user_pass;

	/**
	*
	* @param backup_log [TRUE,FALSE]
	* 
	* (param used is enable means to write file to db_back_up_log)
	*/
	
	private $backup_log;

	/**
	*
	* @param notice_to_owner [TRUE,FALSE]
	* 
	* (param used is enable means to send mail is backup is done)
	*/
	
	private $notice_to_owner;

	/**
	*
	* @param owner_mail_id 
	* 
	* (param notice_to_owner == TRUE must required mail id)
	*/
	
	private $owner_mail_id;

	/**
	*
	* @param how_many_backup
	* @var   int 
	* (param how_many_backup == 1 is been default)
	*
	*/
	
	private $how_many_backup;

	/**
	*
	* @param $dbs_connection
	* @var   int 
	* (param how_many_backup == 1 is been default)
	*
	*/

	private $dbs_connection;

	function __construct(){

		parent::__construct();

		$this->utility = new utility();

	}

	/**
	*
	* @param _check_set_dir
	* @ checking and setting directory
	* @var BCP_DIR_URL
	*/

	private function _check_set_dir()
	{
		$error = [];

		/* Checking bcp directory */

			if(defined('BCP_DIR_URL')){

				empty(trim(BCP_DIR_URL)) && !is_dir(BCP_DIR_URL) ? $error['backup_directory_url'] = 'Please give valid backup directory' : $this->backup_directory_url = BCP_DIR_URL;

			}else{

				$error['backup_directory_url'] = 'Please define BCP_DIR_URL';
			}

		return $error;
	}

	/**
	*
	* @param _check_set_host
	* @ checking and setting host (test case)
	* @var BCP_HOST
	*/

	private function _check_set_host()
	{

		$error = [];

		/* Checking bcp databases */

			if(defined('BCP_HOST')){

				empty(trim(BCP_HOST)) && !is_dir(BCP_HOST) ? $error['db_user_host'] = 'Please give valid database host' :'';

				if( empty($error['db_user_host']) ){

					$host_checker_report = $this->utility->host_checker(BCP_HOST);

					if(($host_checker_report['status']??false) == false){
						$error['db_user_host']['error']     = 'Host test case failed';
						$error['db_user_host']['test_case'] = $host_checker_report;
					}else{

						$this->db_user_host = BCP_HOST;
					}
				}

			}else{

				$error['db_user_host'] = 'Please define BCP_HOST';
			}

		return $error;
	}

	/**
	*
	* @param _check_set_db_user
	* @ checking and setting dbs user
	* @var BCP_USER
	*/

	private function _check_set_db_user()
	{
		$error = [];

		/* Checking bcp user */

			if(defined('BCP_USER')){

				empty(BCP_USER) ? $error['db_user_name'] = 'Please give database user' : $this->db_user_name = BCP_USER;

			}else{

				$error['db_user_name'] = 'Please define BCP_USER';
			}

		return $error;
	}

	/**
	*
	* @param _check_set_db_pass
	* @ checking and setting dbs user
	* @var BCP_PASS
	*/

	private function _check_set_db_pass()
	{
		$error = [];

		/* Checking bcp user */

			if(defined('BCP_PASS')){

				empty(BCP_PASS) ? $error['db_user_pass'] = 'Please give database user pass' :$this->db_user_pass = BCP_PASS;

			}else{

				$error['db_user_pass'] = 'Please define BCP_PASS';
			}

		return $error;
	}

	/**
	*
	* @param _check_set_dbs_connectivity
	* @ checking and setting dbs user
	* @var BCP_DATABASES
	*/

	private function _check_set_dbs_connectivity()
	{
		$error = [];

		/* Checking bcp databases */

			if(defined('BCP_DATABASES')){

				!is_array(BCP_DATABASES) or empty(BCP_DATABASES) ? $error['database_names'] = 'Please give valid database names' :$this->database_names = BCP_DATABASES;

			}else{

				$error['database_names'] = 'Please define BCP_DATABASES';
			}

		/* Now go for db connectivity test */

			$db = new db($this->db_user_host,$this->db_user_name,$this->db_user_pass);

			$connection = $db->pconnect();

			//Connection test
			if(empty($connection)){

				$error['db_connection'] = "Provided Db credentials cannot connect db";

			}else{

				$all_dbs = $db->db_list();

				//Assign Connection 

				$this->dbs_connection = $connection;

				//if checking your databases is valid or not

				if(count($all_dbs)){

					$available_dbs = [];

					foreach ($all_dbs as $key => $value) {

						if(in_array($value['Database']??'', $this->database_names)){

							$available_dbs[] = $value['Database'];
						}
					}

					$not_available_dbs = array_diff($this->database_names,$available_dbs);

					if(count($not_available_dbs)){

						$error['database_exist_mysql']['error']     = 'Some database does not exist';
						$error['database_exist_mysql']['databases'] = $not_available_dbs;
					}

				}else{

					$error['database_exist_mysql'] = "No databases available in your host";
				}
			}

		/* Error return */

			return $error;
	}

	/**
	*
	* @param _check_set_logs
	* @ checking and setting logs
	* @var BCP_LOGS
	*/

	private function _check_set_logs()
	{
		$error = [];

		/* Checking bcp log */

			if(defined('BCP_LOGS')){

				trim(BCP_LOGS) == '' or is_bool(BCP_LOGS) == FALSE ? $error['backup_log'] = 'Please give TRUE OR FALSE in BCP_LOGS' :$this->backup_log = BCP_LOGS;

			}else{

				$error['backup_log'] = 'Please define BCP_LOGS';
			}

		return $error;
	}

	/**
	*
	* @param _check_set_logs
	* @ checking and setting logs
	* @var BCP_NOTICE BCP_NOTICE_EMAIL
	*/

	private function _check_set_notice_email()
	{
		$error = [];

		/* Checking bcp notice */

			if(defined('BCP_NOTICE')){

				trim(BCP_NOTICE) == '' or is_bool(BCP_NOTICE) == FALSE  ? $error['notice_to_owner'] = 'Please give TRUE OR FALSE in BCP_NOTICE' :$this->notice_to_owner = BCP_NOTICE;

			}else{

				$error['notice_to_owner'] = 'Please define BCP_NOTICE';
			}

		/* Checking & set notice email*/

			if(defined('BCP_NOTICE_EMAIL')){

				if(empty($error['notice_to_owner'])){

					trim(BCP_NOTICE_EMAIL) == '' && BCP_NOTICE == TRUE ? $error['owner_mail_id'] = 'BCP_NOTICE is enabled, Please give email id' :$this->owner_mail_id = BCP_NOTICE_EMAIL;
	
					/* Checking is valid email id */
					if(empty($error['owner_mail_id'])){

						$email_checker_result = $this->utility->email_checker($this->owner_mail_id);

						if($email_checker_result==FALSE){

							$error['owner_mail_id'] = 'Provided email id is not valid, please give valid email Id';
						}
					}				
				}

			}else{

				$error['owner_mail_id'] = 'Please define BCP_NOTICE';
			}

		return $error;
	}

	/**
	*
	* @param _check_set_logs
	* @ checking and setting logs
	* @var BCP_LOGS
	*/


	private function _config_checker($return = false)
	{

		/* Calling one by one */
		$error   = [];

		/* Calling Directory setup */
		$error   = array_merge($error,$this->_check_set_dir());

		/* Calling Host (test case)*/
		$error   = array_merge($error,$this->_check_set_host());

		/* Calling DB user */
		$error   = array_merge($error,$this->_check_set_db_user());

		/* Calling DB pass */
		$error   = array_merge($error,$this->_check_set_db_pass());

		/* Calling DBS Connectivity (its setted connection if no error return ) */
		$error   = array_merge($error,$this->_check_set_dbs_connectivity());

	}

	/**
	*
	*  run_backup ()
	* @param [configured]
	*
	*/

	public function run_backup()
	{
		
		print_r($this->_config_checker());

		// $db = new db('localhost','root','sd');

		// print_r($db->db_list(true));
	}
}