<?php

/**
 * Utility 
 *
 * Uses to serveral usefull functions to do basic things
 * (folder_scanner)
 *
 *
 * @package	Streamer
 * @author	Subash Diego
 * @version 0.1
 * @link http://www.facebook.com/schandar
 */

namespace sdp\packages\Backup;

use sdp\packages\Config.php;

class Utility {

	/**
	*
	* @param folder_url
	* To scan the foler using folder_scanner
	*
	*/
	private $folder_url;

	function __construct()
	{
		
	}

	/**
 	* Class folder_scanner
	*
	* (This scanner is scan given directory with subdirectory list into 
	* array )
	*
	* @param #path
	* @return array
	* i.e ['basename','extension',filename,fullname,path]
	*/

	public function folder_scanner($dir ='',&$list = [])
	{

		if(is_dir($dir)){

			$files = array_diff(scandir($dir), array('..', '.'));

		    foreach ( $files as $file_or_folder ){
		     
		        $list[] = array_merge(pathinfo($file_or_folder),['path' => substr($dir,-1) == '/' ? $dir : $dir.'/']);

		        $c_file_or_folder_path = substr($dir,-1) == '/' ? $dir.$file_or_folder.'/' : $dir.'/'.$file_or_folder.'/';

	            if( is_dir($c_file_or_folder_path)){

	                folder_scanner($c_file_or_folder_path,$list);
	            }
		    }
		}

		return $list;
	}

	/**
	 * Tests for file writability
	 *
	 * is_writable() returns TRUE on Windows servers when you really can't write to
	 * the file, based on the read-only attribute. is_writable() is also unreliable
	 * on Unix servers if safe_mode is on.
	 *
	 * 
	 * @param	string
	 * @return	bool
	 */

	function is_really_writable($file)
	{
		// If we're on a Unix server with safe_mode off we call is_writable
		if (! ini_get('safe_mode'))
		{
			return is_writable($file);
		}

		/* For Windows servers and safe_mode "on" installations we'll actually
		 * write a file then read it. Bah...
		 */
		if (is_dir($file))
		{
			$file = rtrim($file, '/').'/'.md5(mt_rand());
			if (($fp = @fopen($file, 'ab')) === FALSE)
			{
				return FALSE;
			}

			fclose($fp);
			@chmod($file, 0777);
			@unlink($file);
			return TRUE;
		}
		elseif ( ! is_file($file) OR ($fp = @fopen($file, 'ab')) === FALSE)
		{
			return FALSE;
		}

		fclose($fp);
		return TRUE;
	}

	/**
	 * host_checker
	 *
	 * HOST CHACKER is doing to check the valid ip or not
	 *
	 * 
	 * @param	string[32]
	 * @return	array
	 */

	function host_checker($host = ''){

		$report = ['status' => FALSE];

		if(trim($host)!=''){

			/* Progess host */

			$host_length = strlen($host);

			if($host_length > 15){

				//ipv6
				$report['data']['type'] = 'ipv6';

				if (filter_var($host, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {

					$report['data']['is_valid'] = true;

				} else {

					$report['data']['is_valid'] = false;
				}

			}else{

				//ipv4
				$report['data']['type'] = 'ipv4';

				if (filter_var($host, FILTER_VALIDATE_IP)) {

					$report['data']['is_valid'] = true;

				} else {

					$report['data']['is_valid'] = false;
				}
			}

			/* ip test case */

			if( ($report['data']['is_valid']??false) ){

				$ip =  $host;

				//checking windows service 
				exec("ping -n 3 $ip", $output, $status);

				if($status){ //if system is not windows

					//checking linux service 
					exec("ping -c 3 $ip", $output, $status);

					$report['status'] = $status==0 ? true : false;

					$report['data']['pings'] = $output;

				}else{

					$report['data']['pings'] = $output;

					$report['status'] = $status==0 ? true : false;
				}
			}
		}

		return $report;
	}

	/**
	 * email_checker
	 *
	 * (email_checker) checks email id and return is valid email or not
	 *
	 * 
	 * @param	string[100]
	 * @return	bool
	 */

	function email_checker($email =''){

		$report = false;

		if(trim($email)!=''){

			/* Checking is valid email (SANITIZE) */

			if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			 	
			 	$report = true;
			}
		}

		return $report;
	}

}