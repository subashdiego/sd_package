<?php

/**
 * Backup 
 *
 * Uses to do database backup using (mysql (cmd))
 * 
 *
 *
 * @package	Streamer
 * @author	Subash Diego
 * @version 0.1
 * @link http://www.facebook.com/schandar
 */

namespace sdp\packages\Curl;

use sdb\packages\Config;

class Curl {

    /**
     * @var url
     * @uses url is used to request url
     */

    public $url;

    /**
     * @var header
     * @uses header is used to request header
     */
    
    public $header;

    /**
     * @var body
     * @uses body is used to request body
     */
    
    public $body;

    /**
     * @var method
     * @uses method is used to request method
     */
    
    public $method;

    /**
     * @var max_execution_time
     * @uses max_execution_time is used to request max_execution_time
     */
    
    public $max_execution_time = 30;
    
    /**
     * @var response
     * @uses max_execution_time is used to request type
     */
    
    public $response = [
                        'error'     => '',
                        'response'  => []
                    ];
                
    function curl($parms = array()){

        if(!empty($parms['url'])){

            $fields   = isset($parms['parms']) && count($parms['parms']) ? $parms['parms'] : array();
            $headers  = isset($parms['headers']) && count($parms['headers']) ? $parms['headers'] : array();
            $url      = $parms['url']; //makesure url must be close
            $type     = $parms['type']==''?'GET':$parms['type'];$type = strtoupper($type); //makesure url must be close

            // is Clear URL
            substr($url,-1)!="/" ? $url = $url.'/' : '';

            //ROLLING AND GETTING HEADERS
                
                $arranged_header = array();

                if(count($headers)){
                    foreach ($headers as $key => $value) {
                        $arranged_header[] = $key.':'.$value;
                    }
                }

                $arranged_header[] = "Cache-Control: no-cache";

            $curl     = curl_init();
            $response = array();
            //TYPE BASED CURL REQUEST
                if($type=="GET"){
                    //ROLLING parms GETTING INTO GET ACTION 
                        $parms_maker = '';

                        if(count($fields)){
                            foreach ($fields as $key => $value) {
                                $parms_maker .= $key.'='.$value.'&'; 
                            }
                            $parms_maker = '?'.$parms_maker;
                        }

                        //print_r($parms_maker);die;

                    curl_setopt_array($curl, array(
                      CURLOPT_URL => $url.$parms_maker,
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => "",
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 60,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => $type,
                      CURLOPT_HTTPHEADER => $arranged_header,
                    ));

                    $response = curl_exec($curl);
                    $err =      curl_error($curl);
                                curl_close($curl);
                    
                }

                if($type=="POST"){

                    curl_setopt($curl, CURLOPT_URL,$url);
                    curl_setopt($curl, CURLOPT_POST, 1);
                    curl_setopt($curl, CURLOPT_ENCODING, "");
                    curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
                    curl_setopt($curl, CURLOPT_TIMEOUT, 60);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $arranged_header);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    // CURL INPUT
                    curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query( $fields ));
                    
                    $response = curl_exec($curl);
                    $err =      curl_error($curl);
                                curl_close($curl);

                }

            //END CURL GET OR POST

            return json_decode($response,true);
        }
    }
}